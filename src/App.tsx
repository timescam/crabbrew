import "./App.css";
import { LeftNav } from "./components/left-nav";
// import TitleBar from "./components/titlebar";
import { Box, Text } from "dracula-ui";
import { TopNav } from "./components/top-nav";
import { Content } from "./components/content";
import { useBetween } from "use-between"; 
import { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";

const sharedState = ()	=> {
	const [selectedEntry, setEntry] = useState<String>("");
	const [entryInfo, setEntryInfo] = useState<any>("Hello World!");
	return {
		selectedEntry,
		setEntry,
		entryInfo,
		setEntryInfo,
	}
}
const useSharedState = () => useBetween(sharedState);

function App() {
	return (
		<div
			className="container-fluid overflow-hidden"
			style={{ height: "100vh" }}
		>
			<div className="row">
				<div
					className="col-3 drac-bg-black"
					style={{ maxWidth: 250 }}
				>
					<LeftNav className="pt-2" state={useSharedState()}></LeftNav>
				</div>
				<div className="col-9">
					<TopNav className="sticky-top pt-2" state={useSharedState()}></TopNav>
					
					<Content className="p-3" state={useSharedState()}></Content>
				</div>
			</div>
		</div>
	);
}

export default App;
