import React from "react";
import { Button, Heading, Input, Box, Text } from "dracula-ui";
import { invoke } from "@tauri-apps/api/tauri";
import Fuse from "fuse.js";
import "bootstrap/dist/css/bootstrap.min.css";
import { stat } from "fs";

export interface LeftNavProps {
	className?: string;
	state?: any;
}

export const LeftNav: React.FC<LeftNavProps> = ({
	className = "",
	state = null,
}) => {
	const [listFormulas, setFormulas] = React.useState<any>([]);
	const [listCasks, setCasks] = React.useState<any>([]);
	const [listTaps, setTaps] = React.useState<any>([]);
	const [installedFormulas, setInstalledFormulas] = React.useState<any>([]);
	const [installedCasks, setInstalledCasks] = React.useState<any>([]);
	const [addedTaps, setAddedTaps] = React.useState<any>([]);
	const { selectedEntry, setEntry, entryInfo, setEntryInfo } = state;

	// only run once
	React.useEffect(() => {
		let ignore = false;
		invoke("brew_list", {
			options: "--formula",
		}).then((formula: any) => {
			formula = formula.split("\n");
			formula.pop();
			setFormulas(formula);
			setInstalledFormulas(formula);
		});

		invoke("brew_list", {
			options: "--cask",
		}).then((cask: any) => {
			cask = cask.split("\n");
			cask.pop();
			setCasks(cask);
			setInstalledCasks(cask);
		});

		invoke("brew_tap").then((tap: any) => {
			tap = tap.split("\n");
			tap.pop();
			setTaps(tap);
			setAddedTaps(tap);
		});
		return () => {
			ignore = true;
		};
	}, []);

	function filterItems(arr: Array<any>, query: string) {
		if (query === "") return arr;
		// let filtered = arr.filter((el) =>
		// 	el.toLowerCase().includes(query.toLowerCase())
		// );
		const fuse = new Fuse(arr, { findAllMatches: true });
		let filtered = fuse.search(query);
		if (filtered.length > 0) {
			return filtered.map((el) => el.item);
		}
		return [];
	}

	function useFilter(filter: string) {
		let filteredFormulas = filterItems(installedFormulas, filter);
		let filteredCasks = filterItems(installedCasks, filter);
		let filteredTaps = filterItems(addedTaps, filter);
		setFormulas(filteredFormulas);
		setCasks(filteredCasks);
		setTaps(filteredTaps);
	}

	function fetchingInfo(entry: string) {
		console.log(entry)
		setEntry(entry);
		setEntryInfo("Loading...");
		invoke("brew_info", { query: entry }).then((info: any) => {
			setEntryInfo(JSON.parse(info));
		});
	}

	return (
		<Box
			className={className + " overflow-scroll mt-2"}
			style={{ wordBreak: "keep-all", height: "100vh" }}
		>
			<Input
				placeholder="Search Packages"
				className="sticky-top mb-2"
				onKeyUp={(event) => useFilter(event.target.value)}
			/>
			<Heading size="md">Installed Packages</Heading>
			{listFormulas.map((formula: any) => {
				return (
					<div>
						<Button
							variant="ghost"
							color="purple"
							p="sm"
							m="xxs"
							onClick={() => fetchingInfo(formula)}
							disabled={formula === selectedEntry}
						>
							{formula}
						</Button>
					</div>
				);
			})}

			<br />
			<Heading size="md">Installed Casks</Heading>

			{listCasks.map((cask: any) => {
				return (
					<div>
						<Button
							variant="ghost"
							color="pink"
							p="sm"
							m="xxs"
							onClick={() => fetchingInfo(cask)}
							disabled={cask === selectedEntry}
						>
							{cask}
						</Button>
					</div>
				);
			})}

			<br />
			<Heading size="md">Added Taps</Heading>

			{listTaps.map((tap: any) => {
				return (
					<div>
						<Button
							variant="ghost"
							color="cyan"
							p="sm"
							m="xxs"
							// onClick={() => fetchingInfo(tap)}
							disabled={tap === selectedEntry}
						>
							{tap}
						</Button>
					</div>
				);
			})}
			<br />
			<Text align="center" as="p" size="xs" color="grey">
				crabbrew is not affiliated with{" "}
				<a href="https://brew.sh/">Homebrew</a>
				<br />
				This project is licensed under the BSD 2-Clause "Simplified"
				License.
				<br />
				See{" "}
				<a href="https://gitlab.com/timescam/crabbrew/-/raw/main/LICENSE">
					LICENSE
				</a>{" "}
				for more information.
			</Text>
		</Box>
	);
};
