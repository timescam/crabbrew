import React from "react";
import { Card, Heading, Divider, Button, Text, heights } from "dracula-ui";
import InfoIcon from "@mui/icons-material/Info";
import { WebviewWindow } from "@tauri-apps/api/window";
import "bootstrap/dist/css/bootstrap.min.css";
// @ts-ignore
import JSONViewer from "react-json-viewer";

export interface ContentProps {
	className?: string;
	state?: any;
}

export const Content: React.FC<ContentProps> = ({
	className = "",
	state = null,
}) => {
	const { entryInfo } = state;
	let display: any;
	if (typeof entryInfo === "string") {
		display = <Text>{entryInfo}</Text>;
	} else if (typeof entryInfo === "object") {
		// console.log(entryInfo);
		let formula_display = <></>;
		if (entryInfo.formulae.length > 0) {
			formula_display = (
				<>
					<Heading>Formulae</Heading>
					{/* {entryInfo.formulae.map((formula: any) => {
						return (
							<Card
								variant="subtle"
								borderColor="cyan"
								p="xxs"
								display="inline-flex"
							>
								<Text color="cyan">{formula}</Text>
							</Card>
						);
					})} */}
					<Button
						variant="ghost"
						color="cyan"
						p="xxs"
						m="xxs"
						onClick={() => {
							// toggle formula detail visibility by className
							let formula_detail =
								document.getElementById("formula_detail");
							if (formula_detail) {
								if (formula_detail.className === "d-none") {
									formula_detail.className = "";
								} else {
									formula_detail.className = "d-none";
								}
							}
						}}
					>
						Details <InfoIcon></InfoIcon>
					</Button>
					<Card id="formula_detail" className="d-none">
						<JSONViewer json={entryInfo.formulae[0]} />
					</Card>

					<Divider />
				</>
			);
		}
		let cask_display = <></>;
		if (entryInfo.casks.length > 0) {
			cask_display = (
				<>
					<Heading>Casks</Heading>
					{/* {entryInfo.casks.map((cask: any) => {
						return (
							<Card
								variant="subtle"
								borderColor="cyan"
								p="xxs"
								display="inline-flex"
							>
								<Text color="cyan">{cask}</Text>
							</Card>
						);
					})} */}
					<Divider />
				</>
			);
		}
		display = (
			<Text>
				{formula_display}
				{cask_display}
				{/* <Card
					variant="subtle"
					borderColor="cyan"
					p="xxs"
					display="inline-flex"
				>
					<Text color="cyan">{entryInfo[0].tap}</Text>
				</Card> */}
				{/* <Heading>{entryInfo[0].name}</Heading> */}
			</Text>
		);
	} else {
		display = <Text>Unexpected Error</Text>;
	}
	return (
		<div
			className={className + " overflow-scroll"}
			style={{ height: "93vh" }}
		>
			{display}
		</div>
	);
};
