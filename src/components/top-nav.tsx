import React from "react";
import { Heading, Box, Button, Divider, Text } from "dracula-ui";
import AddIcon from "@mui/icons-material/Add";
import ReplayIcon from "@mui/icons-material/Replay";
import SportsBarIcon from "@mui/icons-material/SportsBar";
import "bootstrap/dist/css/bootstrap.min.css";

export interface TopNavProps {
	className?: string;
	state?: any;
}

export const TopNav: React.FC<TopNavProps> = ({
	className = "",
	state = null,
}) => {
	const { selectedEntry } = state;
	return (
		<Box p="none" className={className + " row align-items-center"}>
			<Heading size="xl" className="col-8">
				{selectedEntry === "" ? "crabbrew🦀" : selectedEntry}
			</Heading>
			<div className="col-4 row">
				<div className="col-sm-4 pt-1 pb-1">
					<Button variant="ghost" color="yellow" onClick={reload}>
						<ReplayIcon></ReplayIcon>
					</Button>
				</div>
				<div className="col-sm-4 pt-1 pb-1">
					<Button variant="ghost" color="orange">
						<SportsBarIcon></SportsBarIcon>
					</Button>
				</div>
				<div className="col-sm-4 pt-1 pb-1">
					<Button variant="ghost" color="green">
						<AddIcon></AddIcon>
					</Button>
				</div>
			</div>
			<Text>
				<Divider />
			</Text>
		</Box>
	);
};
function reload(): void {
	window.location.reload();
}
