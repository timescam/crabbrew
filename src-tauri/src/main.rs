// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

#[tauri::command]
fn brew_list(options: String) -> String {
    // println!("options: {}", options);
    // if options != "--cask" && options != "--formula" run brew list with no args
    // else run brew list with options
    use std::process::Command;
    if options != "--cask" && options != "--formula" {
        let output = Command::new("brew").arg("list").arg("-1").output().expect("");
        let output = String::from_utf8_lossy(&output.stdout);
        let output = output.to_string();
        // println!("output:\n {}", output);
        output.into()
    } else {
        let output = Command::new("brew").arg("list").arg(options).arg("-1").output().expect("");
        let output = String::from_utf8_lossy(&output.stdout);
        let output = output.to_string();
        output.into()
    }
}

#[tauri::command]
async fn brew_tap() -> String {
    use std::process::Command;
    let output = Command::new("brew").arg("tap").output().expect("");
    let output = String::from_utf8_lossy(&output.stdout);
    let output = output.to_string();
    output.into()
}

#[tauri::command]
async fn brew_info(query: String) -> String {
	use std::process::Command;
	let output = Command::new("brew").arg("info").arg("--json=v2").arg(query).output().expect("");
	let output = String::from_utf8_lossy(&output.stdout);
	let output = output.to_string();
	output.into()
}

fn main() {
    tauri::Builder
        ::default()
        .invoke_handler(tauri::generate_handler![brew_list, brew_tap, brew_info])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}